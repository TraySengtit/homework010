import React from 'react'
import Menu from './component/Menu'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'
import Author from './view/Author'
import Home from './view/Home'
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <Router>
      <Menu />
      <Switch>
        <Route exact path='/author' component={Author} />
        <Route path='/home' component={Home} />
        <Route path='/*' render={() => <h1>404 Not Found</h1>} />
      </Switch>

    </Router>
  )
}

export default App
