import React,{useState, useEffect} from 'react'
import {Card, Button, Container, Row, Col} from 'react-bootstrap'
import { useHistory } from 'react-router'
import { fetchAuthor } from '../service/author_service'

function Home() {

    const [authors, setAuthor] = useState([])
    let history = useHistory()

    useEffect(async() => {
        let author = await fetchAuthor()
        setAuthor(author)
    }, []) 

    let authorCard = authors.map((author)=>
        <Col  md={3} key={author._id}>
        <Card>
            <Card.Img variant="top" src={author.image} />
            <Card.Body>
                <Card.Title>{author.name}</Card.Title>
                <Card.Text>
                {author.email}
                </Card.Text>
                <Button variant="primary">Go somewhere</Button>
            </Card.Body>
            </Card>
        </Col>
    )

    return (
        <div>
            <Container>
                <h2>All Author</h2>
                <Row>                    
                    {authorCard}
                </Row>
            </Container>
        </div>
    )
}

export default Home
