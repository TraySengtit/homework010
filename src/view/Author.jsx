import React, {useState, useEffect} from 'react'
import { Container, Table, Row, Col, Form, Button, Card} from 'react-bootstrap'
import { fetchAuthor, postAuthor, uploadImage, deleteAuthor, updateAuthor } from '../service/author_service'
import { useParams } from "react-router";

function Author() {
    const [authors, setAuthor] = useState([])
    const {id} = useParams()

    useEffect(async() => { 
        let author = await fetchAuthor()
        setAuthor(author)
    }, []) 

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    
    const clearForm = (e) => {
        setName("");
        setEmail("");
        setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
        setImageFile("");
    };


    const onAdd = async(e)=>{
        e.preventDefault()
        let author= {
            name,email
        }
        if(imageFile){
            let url = await uploadImage(imageFile)
            author.image = url
        }
        postAuthor(author).then(message=>alert(message))
        clearForm(e)
    }

    const onDelete = (id)=>{
        deleteAuthor(id).then((message)=>{

        let newAuthor = authors.filter((author)=>author._id !== id)
        setAuthor(newAuthor)

        alert(message)
        }).catch(e=>console.log(e))
    }

    const onUpdate = async(e, id, name, email, imageURL)=>{
    e.preventDefault()
        let author = 
            setName(name)
            setEmail(email)
            setImageURL(imageURL);
        if(imageFile){
        let url = await uploadImage(imageFile)
        author.image = url
        }
        updateAuthor(id,author).then(message=>alert(message))
    // let author = {
    //     name,email
    // }
    if(imageFile){
        let url = await uploadImage(imageFile)
        author.image = url
    }
    //updateAuthor(id,author).then(message=>alert(message))
}

    return (
    <Container>
        
        <h1 className="my-2">{id?"Update Author":"Add Author"}</h1>
        <Row>
        <Col md={8}>
            <Form>
            <Form.Group controlId="title">
                <Form.Label>Username</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Username" 
                value={name}
                onChange={(e)=>setName(e.target.value)}
                />
                <Form.Text className="text-muted">
                </Form.Text>
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Email</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Email" 
                value={email}
                onChange={(e)=>setEmail(e.target.value)}
                />
                
            </Form.Group>
            <Button 
                variant="primary" 
                type="submit"
                onClick={id? onUpdate:onAdd}
            >
                {id? 'Update':'Add'}
            </Button>
            </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image"
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
        </Row>
        <Table striped bordered hover>
            <thead>
            <tr>
                <th style={{width: '50px'}}>#</th>
                <th style={{width: '300px'}}>Username</th>
                <th style={{width: '450px'}}>Email</th>
                <th style={{width: '150px'}}>Image</th>
                <th style={{width: '200px'}}>Action</th>
            </tr>
            </thead>
            <tbody>
                {authors.map((author, index)=>{
                    return(
                        <tr key={author._id}>
                            <td>{index+1}</td>
                            <td>{author.name}</td>
                            <td>{author.email}</td>
                            <td>
                                <Card.Img style={{ objectFit: 'cover', height: "80px", width: '150px'}} variant="top" src={author.image} />
                            </td>
                            <td>
                                <Button variant="warning" 
                                    onClick={(e)=>onUpdate(
                                        e,
                                        author._id,
                                        author.name,
                                        author.email,
                                        author.image        

                                    )}
                                >    
                                Edit</Button>{' '}

                                <Button variant="danger" onClick={()=>onDelete(author._id)}>Delete</Button>
                            </td>
                        </tr>
                    )
                })}    
            </tbody>
        </Table>
    </Container>
    );

}

export default Author
